from typing import List, Set, Tuple

Point = Tuple[int, int]


def read_wires_from_file(input_file: str) -> Tuple[List[str], List[str]]:
    with open(input_file) as f:
        wire1_instr = f.readline()
        wire2_instr = f.readline()

    wire1 = wire1_instr.strip().split(",")
    wire2 = wire2_instr.strip().split(",")

    return wire1, wire2


def decompose_instruction(instr: str) -> Tuple[str, int]:
    """Takes in instruction, and returns direction and distance

    Ex: Takes in "U43", and returns ("U", 43)
    """
    direction = instr[0]
    distance = int(instr[1:])

    return direction, distance


def get_new_point(current_point: Point, instr: str) -> Point:
    direction, distance = decompose_instruction(instr)

    if direction == "U":
        new_point = (current_point[0], current_point[1] + distance)
    elif direction == "D":
        new_point = (current_point[0], current_point[1] - distance)
    elif direction == "R":
        new_point = (current_point[0] + distance, current_point[1])
    elif direction == "L":
        new_point = (current_point[0] - distance, current_point[1])
    else:
        raise ValueError(f"Invalid instruction: '{instr}'")

    return new_point


def get_points_in_range(one: Point, two: Point) -> Set[Point]:
    low, high = sort_points({one, two})
    points_range = {low, high}

    if low[1] == high[1]:
        # change along x
        y = low[1]
        for x in range(low[0] + 1, high[0]):
            points_range.add((x, y))
    elif low[0] == high[0]:
        # change along y
        x = low[0]
        for y in range(low[1] + 1, high[1]):
            points_range.add((x, y))
    else:
        # cannot change both at once
        raise ValueError(f"Point must share x- or y-value: {one}, {two}")

    return points_range


def sort_points(points: Set[Point]) -> List[Point]:
    return sorted(points, key=lambda x: (x[0], x[1]))


class Wire:
    def __init__(self):
        self.current_point: Point = (0, 0)
        self.all_points: Set[Point] = set()

    def merge_points(self, points: Set[Point]) -> None:
        self.all_points.update(points)

    def add_wire_length(self, instr: str) -> List[Point]:
        current_point = self.current_point
        new_point = get_new_point(current_point, instr)
        points = get_points_in_range(current_point, new_point)

        self.merge_points(points)
        self.current_point = new_point

        return sort_points(points)

    def __repr__(self):
        return f"Wire(all_points={sort_points(self.all_points)})"


def calc_distance_from_origin(point: Point):
    # returns the Manhattan distance from (0,0) to (x,y)
    x = point[0]
    y = point[1]

    return abs(x) + abs(y)


def get_min_distance_for_points(crossed_points: Set[Point]) -> float:
    min_distance = float("inf")
    for point in crossed_points:
        if point == (0, 0):
            continue

        distance = calc_distance_from_origin(point)
        if distance < min_distance:
            min_distance = distance

    return min_distance


def crossed_wires(input_file: str) -> float:
    wire_tangle_1, wire_tangle_2 = read_wires_from_file(input_file)

    wire1 = Wire()
    wire2 = Wire()

    crossed_points = set()

    for instr in wire_tangle_1:
        # build wire1
        wire1.add_wire_length(instr)

    for instr in wire_tangle_2:
        # build wire2, and find any intersections with wire1
        new_points = wire2.add_wire_length(instr)

        intersections = [p for p in new_points if p in wire1.all_points]
        crossed_points.update(intersections)

    return get_min_distance_for_points(crossed_points)


# min_dist = crossed_wires("test_input_0.txt")
# min_dist = crossed_wires("test_input_1.txt")
# min_dist = crossed_wires("test_input_2.txt")
min_dist = crossed_wires("input.txt")

print(min_dist)
