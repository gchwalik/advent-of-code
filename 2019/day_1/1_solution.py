import math


def get_module_fuel(mass: int) -> int:
    return math.floor(mass / 3) - 2


def total_fuel(input_file: str) -> int:
    total = 0
    with open(input_file) as f:
        while True:
            module_mass = int(f.readline())
            if not module_mass:
                break

            total += get_module_fuel(module_mass)

    return total


fuel = total_fuel("input.txt")
# fuel = total_fuel("1_test_input.txt")
print(fuel)
