from typing import List


def read_file(input_file: str) -> List[int]:
    with open(input_file) as f:
        int_codes = f.read()

    int_codes = int_codes.strip()
    individual_int_codes = int_codes.split(",")

    return [int(x) for x in individual_int_codes]


def opcode_1(x: int, y: int, write: int, memory: List[int]) -> None:
    """Adds together x and y and stores in memory at address"""
    memory[write] = memory[x] + memory[y]


def opcode_2(x: int, y: int, write: int, memory: List[int]) -> None:
    """Adds together x and y and stores in memory at address"""
    memory[write] = memory[x] * memory[y]


def intcode(input_file: str) -> int:
    memory = read_file(input_file)
    memory[1] = 12
    memory[2] = 2

    op_idx = 0
    while op_idx < len(memory):
        opcode = memory[op_idx]
        if opcode == 99:
            break

        x = memory[op_idx + 1]
        y = memory[op_idx + 2]
        ww = memory[op_idx + 3]

        if opcode == 1:
            opcode_1(x, y, ww, memory)
        elif opcode == 2:
            opcode_2(x, y, ww, memory)
        else:
            raise ValueError(f"Invalid opcode {opcode}")

        op_idx += 4

    return memory[0]


val = intcode("input.txt")
print(val)
